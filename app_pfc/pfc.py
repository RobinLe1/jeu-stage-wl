import random
import time

result = 0
choix_possible = 0
robot = 0
joueur = 0
score_robot = 0
score_joueur = 0

while result == 0:

    result = 2
    choix_possible = ["pierre","feuille","ciseaux"]
    robot = random.choice(choix_possible)

    while joueur != "pierre" and joueur != "feuille" and joueur != "ciseaux":
        joueur = input("Choix entre pierre, feuille et ciseaux : ")

    time.sleep(0.5)
    print("Le robot a joué",robot)
    time.sleep(0.2)

    if robot == joueur :
        print("Réessayer !")
        joueur = 0
        result = 0

    elif robot == "pierre" and joueur == "ciseaux" or robot == "feuille" and joueur == "pierre" or robot == "ciseaux" and joueur == "feuille":
        score_robot += 1
        print("Arrête le jeu t nul !")

    else:
        score_joueur += 1
        print("Tié le boss mon gaté !")

    if joueur != 0 :
        print("Robot :",score_robot)
        print("Joueur :",score_joueur)

        while result != 0 and result != 1 :
            result = int(input("0 = continuer / 1 = arrêter : "))

        joueur = 0