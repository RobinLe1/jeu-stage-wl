import random

#Initialisation des variables à zéro
nbVictoire = 0

while True :
    #Choix et initialisation des variables de jeu
    limiteRandom = int(input("Choisir le plus grand nombres pouvant être tiré : "))
    coupMax = int(input("Choisir le nombre de coup maximum possible : "))
    nombreJoueur = 0

    while True :

        nombreRobot = random.randint(1,limiteRandom)
        resultat = "defaite"

        #Boucle pour une partie
        for coup in range(coupMax-1):
            while nombreJoueur < 1 or nombreJoueur > limiteRandom :
                nombreJoueur = int(input("\nChoisir un nombre entre 1 et "+str(limiteRandom)+" : "))

            if nombreJoueur == nombreRobot:
                resultat = "victoire"
                nbVictoire += 1
                nombreJoueur = 0
                break
            elif nombreJoueur < nombreRobot:
                print("\n=> Il faut un nombre plus grand !")
            else:
                print("\n=> Il faut un nombre plus petit !")

            nombreJoueur = 0

        #Gestion des résultats
        print("\nResultat :",resultat)
        print("=>La partie s'est jouée en :",coup+1,"coup(s).")

        if resultat == "defaite":
            break

        coupMax -= 1
        print("Nombre de coup maximum pour le prochain tour :",coupMax,"coup(s).")

    #Gestion de nouvelles parties
    rejouer = str(input("\n Voulez-vous rejouer une partie ? (O)ui ou (N)on: "))

    if rejouer == "Oui" or rejouer == "oui" or rejouer == "o" or rejouer == "O":
        print("\nSuper, une autre partie !\n")
    else:
        print("\nD'accord, une autre fois :)\n")
        break

#Affichage résumé de la partie
print("Résumé de partie:\n=> Nombre de vitoire Joueur =",nbVictoire,)