import time

tour = 1
Symb = [" "," "," "," "," "," "," "," "," "]
debut = str(input("Quel joueur commence à jouer ? -"))
if debut == "x" or debut == "X" or debut == "1":
    tour, debut = 1, 1
else:
    tour, debut = 2, 2

nbVicJoueur1 = 0
nbVicJoueur2 = 0
nbEgalite = 0

def affichage_tableau():
    print("\n",Symb[0],"|",Symb[1],"|",Symb[2])
    print("---+---+---")
    print("",Symb[3],"|",Symb[4],"|",Symb[5])
    print("---+---+---")
    print("",Symb[6],"|",Symb[7],"|",Symb[8])

def jouer(tour):

    if tour == 1:
        symbole = "X"
        print("\n=> Tour du joueur X. Entrez un nombre entre 1 et 9")

    else:
        symbole = "O"
        print("\n => Tour du joueur 0. Entrez un nombre entre 1 et 9")
    case = 0

    while case < 1 or case > 9 or Symb[case-1]!=" ": 
        case = int(input("- "))
    Symb[case-1] = symbole
    affichage_tableau()

while True:
    Symb = [" "," "," "," "," "," "," "," "," "]
    affichage_tableau()

    for coup in range (1,10):
        jouer(tour)

        if tour == 1:
            tour = 2
            symbole = "X"
        else:
            tour = 1
            symbole = "O"

        if Symb[0:3] == [symbole, symbole, symbole] or Symb[3:6] == [symbole, symbole, symbole] or Symb[6:9] == [symbole, symbole, symbole] :
            print("\nLe joueur", symbole, "a gagné en horizontal !")
            egalite = 0
            break
        elif Symb[0:10:3] == [symbole, symbole, symbole] or Symb[1:10:3] == [symbole, symbole, symbole] or Symb[2:10:3] == [symbole, symbole, symbole]:
            print("\nLe joueur", symbole, "a gagné en vertical !")
            egalite = 0
            break
        elif Symb[0:10:4] == [symbole, symbole, symbole] or Symb[2:8:2] == [symbole, symbole, symbole]:
            print("\nLe joueur", symbole, "a gagné en diagonal !")
            egalite = 0
            break
        egalite = 1

    if egalite == 1:
        nbEgalite += 1
        print("\nEgalite !")
    elif tour == 1:
        nbVicJoueur2 += 1
    elif tour == 2:
        nbVicJoueur1   += 1

    rejouer = str(input("\nVoulez-vous rejouer une partie ? (O)ui ou (N)on: "))

    if rejouer == "Oui" or rejouer == "oui" or rejouer == "o" or rejouer == "O":
        print("\nSuper, continuons!\n")
    else:
        print("\nD'accord, une autre fois :)\n")
        break

    if debut == 1:
        tour, debut = 2, 2
    else:
        tour, debut = 1, 1

print("Résumé de partie:\n=> Nombre de vitoire du joueur X =",nbVicJoueur1,
"\n=> Nombre de victoire du joueur O =",nbVicJoueur2,
"\n=> Nombre d'égalité =",nbEgalite,
"\n=> Nombre de partie jouée =",nbVicJoueur1+nbVicJoueur2+nbEgalite)